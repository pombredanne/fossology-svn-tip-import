-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 1.0
Source: fcitx
Binary: fcitx
Architecture: any
Version: 1:3.6.2-1
Maintainer: Yu Guanghui <ygh@debian.org>
Uploaders: LI Daobing <lidaobing@debian.org>
Homepage: http://code.google.com/p/fcitx/
Standards-Version: 3.8.3
Build-Depends: debhelper (>= 7), libx11-dev, libxft-dev, x11proto-core-dev, libxpm-dev, automake, autoconf, libxtst-dev
Checksums-Sha1: 
 c2c0fd502fd577fe4785c6ab730ece6c5a7fed68 6306315 fcitx_3.6.2.orig.tar.gz
 b41c17c9a461e740c8749c16de5577aceb324141 9154 fcitx_3.6.2-1.diff.gz
Checksums-Sha256: 
 734329b1dd48528a6ac63e586606e690c6d92f77c4270bbfca0b85c7091c1826 6306315 fcitx_3.6.2.orig.tar.gz
 d1a42b28f31e6f83701823df6f0253c106ae8c054e6e9e099eeee9f89078c11b 9154 fcitx_3.6.2-1.diff.gz
Files: 
 b2eb3b086a0a597dedcde07264085cb6 6306315 fcitx_3.6.2.orig.tar.gz
 6925c9799bf4ee4c21216447268cf001 9154 fcitx_3.6.2-1.diff.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.9 (GNU/Linux)

iEYEARECAAYFAkrPJFYACgkQ5TUK4GCH0vinkQCdEBXA7ZXfpFdADM7kmUZenYg7
INcAmwf//KqLszWlAIr2iBrO0Eyuuj5P
=5ZRt
-----END PGP SIGNATURE-----
