#!/usr/bin/make -f
# -*- makefile -*-
# Sample debian/rules that uses debhelper.
#
# This file was originally written by Joey Hess and Craig Small.
# As a special exception, when this file is copied by dh-make into a
# dh-make output file, you may use that output file without restriction.
# This special exception was added by Craig Small in version 0.37 of dh-make.
#
# Modified to make a template file for a multi-binary package with separated
# build-arch and build-indep targets  by Bill Allombert 2001

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

# This has to be exported to make some magic below work.
export DH_OPTIONS

configure: configure-stamp
configure-stamp:
	dh_testdir
	# Add here commands to configure the package.

	touch configure-stamp


#Architecture
build: build-arch build-indep

build-arch: build-arch-stamp
build-arch-stamp: configure-stamp  

	# Add here commands to compile the arch part of the package.
	$(MAKE) PREFIX=/usr SYSCONFDIR=/etc LOCALSTATEDIR=/var
	touch $@

build-indep: build-indep-stamp
build-indep-stamp: configure-stamp  

	@# this currently doesn't do anything, but in case it does someday
	$(MAKE) PREFIX=/usr SYSCONFDIR=/etc LOCALSTATEDIR=/var build-db
	touch $@

clean: 
	dh_testdir
	dh_testroot
	rm -f build-arch-stamp build-indep-stamp configure-stamp

	$(MAKE) clean

	dh_clean 

install: install-indep install-arch
install-indep:
	dh_testdir
	dh_testroot
	dh_clean -k -i 
	dh_installdirs -i

	$(MAKE) DESTDIR=$(CURDIR)/debian/fossology-db \
           PREFIX=/usr SYSCONFDIR=/etc LOCALSTATEDIR=/var install-db

	dh_install -i

install-arch:
	dh_testdir
	dh_testroot
	dh_clean -k -s 
	dh_installdirs -s

	$(MAKE) DESTDIR=$(CURDIR)/debian/fossology-common \
           PREFIX=/usr SYSCONFDIR=/etc LOCALSTATEDIR=/var \
           install-common install-ui install-cli
	$(MAKE) DESTDIR=$(CURDIR)/debian/fossology-common \
           PREFIX=/usr SYSCONFDIR=/etc LOCALSTATEDIR=/var \
           -C devel install-runtime

	$(MAKE) DESTDIR=$(CURDIR)/debian/fossology-scheduler \
           PREFIX=/usr SYSCONFDIR=/etc LOCALSTATEDIR=/var install-scheduler
	@# install the optional defaults file
	$(MAKE) DESTDIR=$(CURDIR)/debian/fossology-scheduler \
           PREFIX=/usr SYSCONFDIR=/etc LOCALSTATEDIR=/var \
	   -C scheduler install-default
	$(MAKE) DESTDIR=$(CURDIR)/debian/fossology-scheduler-single \
           PREFIX=/usr SYSCONFDIR=/etc LOCALSTATEDIR=/var install-scheduler
	@# install the optional defaults file
	$(MAKE) DESTDIR=$(CURDIR)/debian/fossology-scheduler-single \
           PREFIX=/usr SYSCONFDIR=/etc LOCALSTATEDIR=/var \
	   -C scheduler install-default

	$(MAKE) DESTDIR=$(CURDIR)/debian/fossology-agents \
           PREFIX=/usr SYSCONFDIR=/etc LOCALSTATEDIR=/var install-agents
	$(MAKE) DESTDIR=$(CURDIR)/debian/fossology-agents-single \
           PREFIX=/usr SYSCONFDIR=/etc LOCALSTATEDIR=/var install-agents

	$(MAKE) DESTDIR=$(CURDIR)/debian/fossology-dev \
           PREFIX=/usr SYSCONFDIR=/etc LOCALSTATEDIR=/var \
           -C devel install-dev

	dh_install -s
# Must not depend on anything. This is to be called by
# binary-arch/binary-indep
# in another 'make' thread.
binary-common:
	dh_testdir
	dh_testroot
	dh_installchangelogs 
	dh_installdocs -A -Nfossology-common -Nfossology-dev debian/README.Debian
	dh_installdocs -pfossology-dev -pfossology-common
	dh_installexamples
#	dh_installdebconf	
	dh_installlogrotate	
#	dh_installmime
	DH_OPTIONS= dh_installinit -o -pfossology-scheduler --name=fossology
	DH_OPTIONS= dh_installinit -o -pfossology-scheduler-single --name=fossology
#	dh_installcron -pfossology-db
	dh_installman

# Do the lintian stuff by hand for now so we can depend on older debhelper to
# make backports easier, switch when etch support dropped
#	dh_lintian
	mkdir -p debian/fossology-common/usr/share/lintian/overrides
	cp debian/fossology-common.lintian-overrides debian/fossology-common/usr/share/lintian/overrides/fossology-common
	mkdir -p debian/fossology-agents/usr/share/lintian/overrides
	cp debian/fossology-agents.lintian-overrides debian/fossology-agents/usr/share/lintian/overrides/fossology-agents
	mkdir -p debian/fossology-agents-single/usr/share/lintian/overrides
	cp debian/fossology-agents-single.lintian-overrides debian/fossology-agents-single/usr/share/lintian/overrides/fossology-agents-single

	dh_link
	dh_strip
	dh_compress
	dh_fixperms
	dh_makeshlibs
	dh_installdeb
	dh_shlibdeps
	dh_gencontrol
	dh_md5sums
	dh_builddeb
# Build architecture independant packages using the common target.
binary-indep: build-indep install-indep
	$(MAKE) -f debian/rules DH_OPTIONS=-i binary-common

# Build architecture dependant packages using the common target.
binary-arch: build-arch install-arch
	$(MAKE) -f debian/rules DH_OPTIONS=-s binary-common

binary: binary-arch binary-indep
.PHONY: build clean binary-indep binary-arch binary install install-indep install-arch configure
