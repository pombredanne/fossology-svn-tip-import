ATTACHMENT A

REQUIRED NOTICES

ATTACHMENT A-1

REQUIRED IN ALL CASES

"The contents of this file, or the files included with this file, are subject to the current version of Sun Community Source License for [fill in name of applicable Technology] (the "License"); You may not use this file except in compliance with the License.  You may obtain a copy of the License at http://sun.com/software/communitysource.  See the License for the rights, obligations and limitations governing use of the contents of the file.

The Original and Upgraded Code is [fill in name and version of applicable Technology].  The developer of the Original and Upgraded Code is Sun Microsystems, Inc.  Sun Microsystems, Inc.  owns the copyrights in the portions it created.  All Rights Reserved.

Contributor(s):
_______________________________________________

Associated Test Suite(s) Location:
________________________________

ATTACHMENT A-2

SAMPLE LICENSEE CERTIFICATION

"By clicking the `Agree' button below, You certify that You are a Licensee in good standing under the Sun Community Source License, [fill in applicable Technology and Version]
("License") and that Your access, use and distribution of code and information You may obtain at this site is subject to the License."

ATTACHMENT A-3

REQUIRED STUDENT NOTIFICATION

"This software and related documentation has been obtained by your educational institution subject to the Sun Community Source License, [fill in applicable Technology].  You have been provided access to the software and related documentation for use only in connection with your course work and research activities as a matriculated student of your educational institution.  Any other use is expressly prohibited.

THIS SOFTWARE AND RELATED DOCUMENTATION CONTAINS PROPRIETARY MATERIAL OF SUN MICROSYSTEMS, INC, WHICH ARE PROTECTED BY VARIOUS INTELLECTUAL PROPERTY RIGHTS.

You may not use this file except in compliance with the License.  You may obtain a copy of the License on the web at http://sun.com/software/communitysource."

ATTACHMENT B

Java(tm) 2 SDK Technology

Description of "Technology"

Java(tm) 2 SDK Technology v. 5.0 as described on the Technology Download Site.

ATTACHMENT C

INTERNAL DEPLOYMENT USE

This Attachment C is only effective for the Technology specified in Attachment B, upon execution of Attachment D (Commercial Use License) including the requirement to pay royalties.  In the event of a conflict between the terms of this Attachment C and Attachment D, the terms of Attachment D shall govern.

1.  Internal Deployment License Grant.  Subject to Your compliance with Section 2 below, and Section 8.10 of the Research Use license; in addition to the Research Use license and the TCK license, Original Contributor grants to You a worldwide, non-exclusive license, to the extent of Original Contributor's Intellectual Property Rights covering the Original Code, Upgraded Code and Specifications, to do the following:

a) reproduce and distribute internally, Original Code and Upgraded Code as part of Compliant Covered Code, and Specifications, for Internal Deployment Use,

b) compile such Original Code and Upgraded Code, as part of Compliant Covered Code, and reproduce and distribute internally the same in Executable form for Internal Deployment Use, and

c) reproduce and distribute internally, Reformatted Specifications for use in connection with Internal Deployment Use.

2.  Additional Requirements and Responsibilities.  In addition to the requirements and responsibilities described under Section 3.1 of the Research Use license, and as a condition to exercising the rights granted under Section 3 above, You agree to the following additional requirements and responsibilities:

2.1 Compatibility.  All Covered Code must be Compliant Covered Code prior to any Internal Deployment Use or Commercial Use, whether originating with You or acquired from a third party.  Successful compatibility testing must be completed in accordance with the TCK License.  If You make any further Modifications to any Covered Code previously determined to be Compliant Covered Code, you must ensure that it continues to be Compliant Covered Code.

ATTACHMENT D COMMERCIAL USE LICENSE

1.  Effect.  This Attachment D is effective only if signed below by You and Original Contributor, and applies to Your Commercial Use of Original Code and Upgraded Code.

2.  Term.  Upon execution of this Attachment D by You and Original Contributor, this Commercial Use license shall have an initial term of 3 years and shall automatically renew for additional one year terms unless either party provides notice to the other no less than 60 days prior to an anniversary date.

3.  Commercial Use License Grant.  Subject to Your compliance with Section 4 below, Section 8.10 of the Research Use license, and the TCK license; in addition to the Research Use license, the TCK license, and the Internal Deployment Use license, Original Contributor grants to You a worldwide, non-exclusive, non-transferable license, to the extent of Original Contributor's Intellectual Property Rights covering the Original Code, Upgraded Code and Specifications, to do the following:

a) reproduce and distribute Compliant Covered Code;

b) compile Compliant Covered Code and reproduce and distribute the same in Executable form through multiple tiers of distribution; and

c) reproduce and distribute Reformatted Specifications in association with Compliant Covered Code.

4.  Additional Requirements and Responsibilities.  In addition to the requirements and responsibilities specified in the Research Use license, the TCK license and the Internal Deployment license, and as a condition to exercising the rights granted in Section 3 above, You agree to the following additional requirements and
responsibilities:

a) Distribution of Source Code.  Source Code of Compliant Covered Code may be distributed only to another Licensee of the same Technology.

b) Distribution of Executable Code.  You may distribute the Executable version(s) of Compliant Covered Code under a license of Your choice, which may contain terms different from this License, provided (i) that You are in compliance with the terms of this License, and (ii) You must make it absolutely clear that any terms which differ from this License are offered by You alone, not by Original Contributor or any other Contributor.

c) Branding.  Products integrating Compliant Covered Code used for Commercial Use must be branded with the Technology compliance logo under a separate trademark license required to be executed by You and Original Contributor concurrent with execution of this Attachment D.

5.  Indemnity/Limitation of Liability.  The provisions of Section 7.1 of the Research Use license are superseded by the following:

a) Your Indemnity Obligation.  You hereby agree to defend, at Your expense, any legal proceeding brought against Original Contributor or any Licensee to the extent it is based on a claim:  (i) that the use, reproduction or distribution of any of Your Error Corrections or Shared Modifications is an infringement of a third party trade secret or a copyright in a country that is a signatory to the Berne Convention; (ii) arising in connection with any representation, warranty, support, indemnity, liability or other license terms You may offer in connection with any Covered Code; or (iii) arising from Your Commercial Use of Covered Code, other than a claim covered by Section 5.b) below, or a patent claim based solely on Covered Code not provided by You.  You will pay all damages costs and fees awarded by a court of competent jurisdiction, or such settlement amount negotiated by You, attributable to such claim.

b) Original Contributor's Indemnity Obligation.  Original Contributor will defend, at its expense, any legal proceeding brought against You, to the extent it is based on a claim that Your authorized Commercial Use of Original Code and Upgraded Code is an infringement of a third party trade secret or a copyright in a country that is a signatory to the Berne Convention, and will pay all damages costs and fees awarded by a court of competent jurisdiction, or such settlement amount negotiated by Original Contributor, attributable to such claim.  The foregoing shall not apply to any claims of intellectual property infringement based upon the combination of code or documentation supplied by Original Contributor with code, technology or documentation from other sources.

c) Right of Intervention.  Original Contributor will have the right, but not the obligation, to defend You, at Original Contributor's expense, in connection with a claim that Your Commercial Use of Original Code and Upgraded Code is an infringement of a third party patent and will, if Original Contributor chooses to defend You, pay all damages costs and fees awarded by a court of competent jurisdiction, or such settlement amount negotiated by Original Contributor, attributable to such claim.

d) Prerequisites.  Under Sections 5.b) and c) above, You must, and under Section 5.a) above, Original Contributor or any Licensee must:  (i) provide notice of the claim promptly to the party providing an indemnity; (ii) give the indemnifying party sole control of the defense and settlement of the claim; (iii) provide the indemnifying party, at indemnifying party's expense, all available information, assistance and authority to defend; and (iv) not have compromised or settled such claim or proceeding without the indemnifying party's prior written consent.

e) Additional Remedies.  Should any Original Code, Upgraded Code, TCK, Specifications, or Modifications become, or in the indemnifying party's opinion be likely to become, the subject of a claim of infringement for which indemnity is provided above, the indemnifying party may, at its sole option, attempt to procure on reasonable terms the rights necessary for the indemnified party to exercise its license rights under this License with respect to the infringing items, or to modify the infringing items so that they are no longer infringing without substantially impairing their function or performance.  If the indemnifying party is unable to do the foregoing after reasonable efforts, then the indemnifying party may send a notice of such inability to the indemnified party together with a refund of any license fees received by the indemnifying party from the indemnified party for the infringing items applicable to the indemnified party's future use or distribution of such infringing items, in which case the indemnifying party will not be liable for any damages resulting from infringing activity with respect to the infringing items occurring after such notice and refund.

6.  Support Programs.

Support to You.  Technical support is not provided to You by Original Contributor under this License.  You may contract for one or more support programs from Original Contributor relating to the Technology which are described on the SCSL Webpage.

Customer Support.  You are responsible for providing technical and maintenance support services to Your customers for Your products and services incorporating the Compliant Covered code.

7.  Royalties and Payments.

Technology specified in Attachment B.

Field of Use:____________________

Royalty per Unit $_____________

b) Royalty Payments.  Payment of royalties shall be made quarterly, shall be due thirty (30) days following the end of the calendar quarter to which they relate and shall be submitted with a written statement documenting the basis for the royalty calculation.

c) Taxes.  All payments required by this License shall be made in United States dollars, are exclusive of taxes, and Licensee agrees to bear and be responsible for the payment of all such taxes, including, but not limited to, all sales, use, rental receipt, personal property or other taxes and their equivalents which may be levied or assessed in connection with this License (excluding only taxes based on Original Contributor's net income).  To the extent Licensee is required to withhold taxes based upon Original Contributor's income in any country, You agree to provide Original Contributor with written evidence of such withholding, suitable for Original Contributor to obtain a tax credit in the United States.

d) Records.  You agree to maintain account books and records consistent with Generally Accepted Accounting Principles appropriate to Your domicile, as may be in effect from time to time, sufficient to allow the correctness of the royalties required to be paid pursuant to this License to be determined.

e) Audit Rights.  Original Contributor shall have the right to audit such accounts upon reasonable prior notice using an independent auditor of Original Contributor's choice (the "Auditor").  The Auditor shall be bound to keep confidential the details of Your business affairs and to limit disclosure of the results of any audit to the sufficiency of the accounts and the amount, if any, of a payment adjustment that should be made.  Such audits shall not occur more than once each year (unless discrepancies are discovered in excess of the five percent (5%) threshold set forth in Section 7.f) below, in which case two consecutive quarters per year may be audited).  Except as set forth in Section
7.f) below, Original Contributor shall bear all costs and expenses associated with the exercise of its rights to audit.

f) Payment Errors.  In the event that any errors in payments shall be determined, such errors shall be corrected by appropriate adjustment in payment for the quarterly period during which the error is discovered.  In the event of an underpayment of more than five percent (5%) of the proper amount owed, upon such underpayment being properly determined by the Auditor, You agree to reimburse Original Contributor the amount of the underpayment and all reasonable costs and expenses associated with the exercise of its rights to audit, and interest on the overdue amount at the maximum allowable interest rate from the date of accrual of such obligation.

8.  Notice of Breach or Infringement.  Each party shall notify the other immediately in writing when it becomes aware of any breach or violation of the terms of this License, or when You become aware of any potential or actual infringement by a third party of the Technology or Sun's Intellectual Property Rights therein.

9.  Proprietary Rights Notices.  You may not remove any copyright notices, trademark notices or other proprietary legends of Original Contributor or its suppliers contained on or in the Original Code, Upgraded Code and Specifications.

10.  Notices.  All written notices required by this License must be delivered in person or by means evidenced by a delivery receipt and will be effective upon receipt by the persons at the addresses specified below.

Original Contributor:  			You:

Sun Microsystems, Inc.  		_____________________________

4150 Network Circle 			______________________________

Santa Clara, California 95054 	______________________________

Attn.:  VP, Sun Software and 
Technology Sales				______________________________
cc:  Legal (Software Sales) 

11.  Disclaimer of Agency.  The relationship created hereby is that of licensor and licensee and the parties hereby acknowledge and agree that nothing herein shall be deemed to constitute You as a franchisee of Original Contributor.  You hereby waive the benefit of any state or federal statutes dealing with the establishment and regulation of franchises.

Agreed:
You: 						Original Contributor:

_____________________________ 	Sun Microsystems, Inc.
(Your Name) 
By:_________________________		By:_____________________

Title:_______________________		Title:____________________

Date:______________________ 		Date:____________________

ATTACHMENT E TECHNOLOGY COMPATIBILITY KIT

The following license is effective for the Java(tm)2 SDK Technology Compatibility Kit only upon execution of a separate support agreement between You and Original Contributor (subject to an annual fee) as described on the SCSL Webpage.  The Technology Compatibility Kit for the Technology specified in Attachment B may be accessed at the

Technology Download Site only upon execution of the support agreement.

1.  TCK License.

a) Subject to the restrictions set forth in Section 1.b below and Section 8.10 of the Research Use license, in addition to the Research Use license, Original Contributor grants to You a worldwide, non-exclusive, non-transferable license, to the extent of Original Contributor's Intellectual Property Rights in the TCK (without the right to sublicense), to use the TCK to develop and test Covered Code.

b) TCK Use Restrictions.  You are not authorized to create derivative works of the TCK or use the TCK to test any implementation of the Specification that is not Covered Code.  You may not publish your test results or make claims of comparative compatibility with respect to other implementations of the Specification.  In consideration for the license grant in Section 1.a above you agree not to develop your own tests which are intended to validate conformation with the Specification.

2.  Requirements for Determining Compliance.

2.1 Definitions.

a) "Added Value" means code which:

(i) has a principal purpose which is substantially different from that of the stand-alone Technology;

(ii) represents a significant functional and value enhancement to the Technology;

(iii) operates in conjunction with the Technology; and

(iv) is not marketed as a technology which replaces or substitutes for the Technology.

b) "Java Classes" means the specific class libraries associated with each Technology defined in Attachment B.

c) "Java Runtime Interpreter" means the program(s) which implement the Java virtual machine for the Technology as defined in the Specification.

d) "Platform Dependent Part" means those Original Code and Upgraded Code files of the Technology which are not in a "share" directory or subdirectory thereof.

e) "Shared Part" means those Original Code and Upgraded Code files of the Technology which are identified as "shared" (or words of similar meaning) or which are in any "share"
directory or subdirectory thereof, except those files specifically designated by Original Contributor as modifiable.

f) "User's Guide" means the users guide for the TCK which Sun makes available to You to provide direction in how to run the TCK and properly interpret the results, as may be revised by Sun from time to time.

2.2 Development Restrictions.  Compliant Covered Code:

a) must include Added Value;

b) must fully comply with the Specifications for the Technology specified in Attachment B;

c) must include the Shared Part, complete and unmodified;

d) may not modify the functional behavior of the Java Runtime Interpreter or the Java Classes;

e) may not modify, subset or superset the interfaces of the Java Runtime Interpreter or the Java Classes;

f) may not subset or superset the Java Classes; and

g) may not modify or extend the required public class or public interface declarations whose names begin with "java", "javax", "jini", "net.jini", "sun.hotjava", "COM.sun" or their equivalents in any subsequent naming convention.

2.3 Compatibility Testing.  Successful compatibility testing must be completed by You, or at Original Contributor's option, a third party designated by Original Contributor, to conduct such tests, in accordance with the User's Guide, and using the most current version of the applicable TCK available from Original Contributor one hundred twenty (120) days (two hundred forty [240] days in the case of silicon
implementations) prior to:  (i) Your Internal Deployment Use; and (ii) each release of Compliant Covered Code by You for Commercial Use.  In the event that You elect to use a version of Upgraded Code that is newer than that which is required under this Section 2.3, then You agree to pass the version of the TCK that corresponds to such newer version of Upgraded Code.

2.4 Test Results.  You agree to provide to Original Contributor or the third party test facility if applicable, Your test results that demonstrate that Covered Code is Compliant Covered Code and that Original Contributor may publish or otherwise distribute such test results.

