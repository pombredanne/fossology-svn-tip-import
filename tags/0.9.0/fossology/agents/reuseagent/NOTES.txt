Copyright (C) 2007 Hewlett-Packard Development Company, L.P.

Development notes...

For the C agent:

- Identify function.
  These match the pattern:
	name ( [args] ) { text }

- Need the following:
	char *KeywordC[][] = {"auto","double","int","struct", ..., NULL};
	char *KeywordGeneric[][] = {"COMMENT","NUMBER","STRING","CHAR","PARM","VAR",NULL};
	int GetTokenStart(char *InputString); -- smart enough to identify comments and strings
	int GetTokenEnd(char *InputString);
	int MatchKeyword(char *InputString, int Len, char *KeywordList[]); /* 1 for yes, 0 for no */


- Issue to resolve:
  What if a comment is longer than 255 characters?
  Solution A: Need to use start offset of 0 to indicate a skip...
  This means bSAM will need to change to ignore these long comments.
  Solution B: Use a KeywordGeneric for COMMENT.
  This is only used if the skip is > 255 characters.
  (I like option B.)

  Same issue for strings.

