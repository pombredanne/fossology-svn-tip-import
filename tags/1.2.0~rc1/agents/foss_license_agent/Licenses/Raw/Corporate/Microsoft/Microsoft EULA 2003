Supplemental End-User License Agreement
Windows System Resource Manager for Microsoft Windows Server 2003,
Enterprise Edition and Datacenter Edition
Published: May 7, 2003

IMPORTANT: READ CAREFULLY - The Microsoft software that accompanies this
Supplemental EULA, which includes computer software and may include
associated media, printed materials, "online" or electronic documentation,
and Internet-based services (collectively, the "OS Components"), is subject
to the terms and conditions of the end user license agreement under which
you have licensed the applicable Microsoft software referenced above and
the terms and conditions of this Supplemental EULA. The OS Components are
provided solely to update, supplement, or replace existing functionality of
the copy of the applicable Microsoft software referenced above with which
you acquired the OS Components (any such software referred to here as "OS
Software"). An amendment or addendum to this Supplemental EULA may
accompany the OS Components. YOU AGREE TO BE BOUND BY THE TERMS OF THE
APPLICABLE OS SOFTWARE END USER LICENSE AGREEMENT ("OS SOFTWARE EULA") AND
THIS SUPPLEMENTAL EULA BY INSTALLING, COPYING, OR OTHERWISE USING THE OS
COMPONENTS. IF YOU DO NOT AGREE, DO NOT INSTALL, COPY, OR USE THE OS
COMPONENTS.

IF YOU DO NOT HAVE A VALIDLY LICENSED COPY OF THE APPLICABLE OS SOFTWARE,
YOU ARE NOT AUTHORIZED TO INSTALL, COPY OR OTHERWISE USE THE OS COMPONENTS
AND YOU HAVE NO RIGHTS UNDER THIS SUPPLEMENTAL EULA.

1. General.

1.1 You are granted a license to use the OS Components under the terms and
conditions of the OS Software EULA (which are hereby incorporated by
reference except as set forth below), the terms and conditions set forth in
this Supplemental EULA, and the terms and conditions of any additional end
user license agreement that may accompany the individual OS Components
(each an "Individual EULA"), provided that you comply with all such terms
and conditions. To the extent that there is a conflict among any of these
terms and conditions applicable to the OS Components, the following
hierarchy shall apply: (i) the terms and conditions of the Individual EULA;
(ii) the terms and conditions in this Supplemental EULA; and (iii) the
terms and conditions of the applicable OS Software EULA.

1.2 The OS Components are protected by copyright and other intellectual
property laws and treaties. Microsoft Corporation or its suppliers own the
title, copyright, and other intellectual property rights in the OS
Components. All rights not expressly granted to you in this Supplemental
EULA are reserved. The OS Components are licensed, not sold.

1.3 Capitalized terms used in this Supplemental EULA and not otherwise
defined herein shall have the meanings assigned to them in the applicable
OS Software EULA.

2. Additional Rights and Limitations.

You may (i) reproduce and install a reasonable number of copies of the OS
Components on any Server, or Device, and (ii) use such copies of the OS
Components solely for administering one or more validly licensed copies of
the OS Software that are installed on your Server(s), provided that you use
each such copy of the OS Components in accordance with the terms and
conditions of this Supplemental EULA. This does not relieve you of any
obligations that you may have to purchase any CALs required by the OS
Software EULA for such use.

3. DISCLAIMER OF WARRANTIES

THE LIMITED WARRANTY (IF ANY) INCLUDED IN THE APPLICABLE OS SOFTWARE EULA
APPLIES TO THE OS COMPONENTS PROVIDED THE OS COMPONENTS HAVE BEEN LICENSED
BY YOU WITHIN THE TERM OF THE LIMITED WARRANTY IN THE APPLICABLE OS
SOFTWARE EULA. THIS SUPPLEMENTAL EULA DOES NOT EXTEND THE TIME PERIOD FOR
WHICH THE LIMITED WARRANTY IS PROVIDED.
