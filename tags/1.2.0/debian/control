Source: fossology
Section: utils
Priority: extra
Maintainer: Matt Taggart <taggart@debian.org>
Build-Depends: debhelper (>= 5), postgresql-server-dev-8.4 | postgresql-server-dev-8.3, libmagic-dev, libxml2-dev, libextractor-dev, libtext-template-perl, librpm-dev, subversion
Standards-Version: 3.8.4
Homepage: http://fossology.org

Package: fossology
Architecture: all
Depends: fossology-web-single, ${misc:Depends}
Description: open and modular architecture for analyzing software
 The FOSSology project is a web based framework that allows you to
 upload software to be picked apart and then analyzed by software agents
 which produce results that are then browsable via the web interface.
 Existing agents include license analysis, metadata extraction, and MIME
 type identification.
 .
 This metapackage ensures that the fossology component packages needed
 for a single-system install are installed in the right order. For a
 multi-system install, consult the README.Debian file included in the
 fossology-common package.

Package: fossology-common
Architecture: any
Depends: php5-pgsql, php-pear, php5-cli, ${shlibs:Depends}, ${misc:Depends}
Description: architecture for analyzing software, common files
 The FOSSology project is a web based framework that allows you to
 upload software to be picked apart and then analyzed by software agents
 which produce results that are then browsable via the web interface.
 Existing agents include license analysis, metadata extraction, and MIME
 type identification.
 .
 This package contains the resources needed by all of the other
 fossology components.

Package: fossology-web
Architecture: all
Depends: fossology-agents, apache2, libapache2-mod-php5, ${misc:Depends}
Conflicts: fossology-web-single
Description: architecture for analyzing software, web interface
 The FOSSology project is a web based framework that allows you to
 upload software to be picked apart and then analyzed by software agents
 which produce results that are then browsable via the web interface.
 Existing agents include license analysis, metadata extraction, and MIME
 type identification.
 .
 This package depends on the packages for the web interface.

Package: fossology-web-single
Architecture: all
Depends: fossology-scheduler-single, apache2, libapache2-mod-php5, ${misc:Depends}
Conflicts: fossology-web
Description: architecture for analyzing software, web interface  (single machine case)
 The FOSSology project is a web based framework that allows you to
 upload software to be picked apart and then analyzed by software agents
 which produce results that are then browsable via the web interface.
 Existing agents include license analysis, metadata extraction, and MIME
 type identification.
 .
 This package depends on the packages needed for the web interface and
 to make a single machine install properly.

Package: fossology-scheduler
Architecture: any
Depends: fossology-agents, ${shlibs:Depends}, ${misc:Depends}
Conflicts: fossology-scheduler-single
Description: architecture for analyzing software, scheduler
 The FOSSology project is a web based framework that allows you to
 upload software to be picked apart and then analyzed by software agents
 which produce results that are then browsable via the web interface.
 Existing agents include license analysis, metadata extraction, and MIME
 type identification.
 .
 This package contains the scheduler daemon.

Package: fossology-scheduler-single
Architecture: any
Depends: fossology-agents-single, ${shlibs:Depends}, ${misc:Depends}
Conflicts: fossology-scheduler
Description: architecture for analyzing software, scheduler (single machine case)
 The FOSSology project is a web based framework that allows you to
 upload software to be picked apart and then analyzed by software agents
 which produce results that are then browsable via the web interface.
 Existing agents include license analysis, metadata extraction, and MIME
 type identification.
 .
 This package contains the scheduler daemon and depends on the right
 packages to make a single machine install properly.

Package: fossology-db
Architecture: all
Depends: postgresql-8.4 | postgresql-8.3, ${shlibs:Depends}, ${misc:Depends}
Description: architecture for analyzing software, database
 The FOSSology project is a web based framework that allows you to
 upload software to be picked apart and then analyzed by software agents
 which produce results that are then browsable via the web interface.
 Existing agents include license analysis, metadata extraction, and MIME
 type identification.
 .
 This package contains the database resources and will create a
 fossology database on the system (and requires that postgresql is
 running at install time). If you prefer to use a remote database,
 or want to create the database yourself, do not install this package
 and consult the README.Debian file included in the fossology-common
 package.

Package: fossology-agents
Architecture: any
Depends: fossology-common, binutils, bzip2, cabextract, cpio, sleuthkit, genisoimage | mkisofs, poppler-utils, rpm, upx-ucl, unrar-free, unzip, libextractor-plugins, p7zip, ${shlibs:Depends}, ${misc:Depends}
Suggests: mail-transport-agent
Conflicts: fossology-agents-single
Description: architecture for analyzing software, analysis agents
 The FOSSology project is a web based framework that allows you to
 upload software to be picked apart and then analyzed by software agents
 which produce results that are then browsable via the web interface.
 Existing agents include license analysis, metadata extraction, and MIME
 type identification.
 .
 This package contains the agent programs and their resources.

Package: fossology-agents-single
Architecture: any
Depends: fossology-db, fossology-common, binutils, bzip2, cabextract, cpio, sleuthkit, genisoimage | mkisofs, poppler-utils, rpm, upx-ucl, unrar-free, unzip, libextractor-plugins, p7zip, ${shlibs:Depends}, ${misc:Depends}
Suggests: mail-transport-agent
Conflicts: fossology-agents
Description: architecture for analyzing software, agents (single machine case)
 The FOSSology project is a web based framework that allows you to
 upload software to be picked apart and then analyzed by software agents
 which produce results that are then browsable via the web interface.
 Existing agents include license analysis, metadata extraction, and MIME
 type identification.
 .
 This package contains the agent programs and their resources plus depends
 on fossology-db in order to ensure the database is set up on this system.

Package: fossology-dev
Architecture: any
Depends: postgresql-server-dev-8.4 | postgresql-server-dev-8.3, ${shlibs:Depends}, ${misc:Depends}
Description: architecture for analyzing software, development files
 The FOSSology project is a web based framework that allows you to
 upload software to be picked apart and then analyzed by software agents
 which produce results that are then browsable via the web interface.
 Existing agents include license analysis, metadata extraction, and MIME
 type identification.
 .
 This package contains resources needed to developer with the
 libfossdb and libfossrepo libraries.
