Copyright (C) 2007 Hewlett-Packard Development Company, L.P.

This is a work-in-progress.  DO NOT USE.  (I guarantee that it does not
work right now.)

Much of this directory are temporary files and work notes.
These will be deleted.
And the main files will be renamed to something more useful
than "fc.c" for "filter C and C++ code".

The current game plan is to rewrite all of the perl code
into C and interface it into the database.

Questions? Ask Neal Krawetz.
