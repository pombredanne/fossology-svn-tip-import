FOSSology hacking
=================
Common settings:
For values that you might want to adjust across the whole build tree
the build system uses variables set in Makefile.conf. The build
system uses these variables in all Makefiles and variables are inherited
down the hierarchy. By setting variables on the make command line you
can easily change things in the build


CFLAGS:
The default CFLAGS for all C compilation is "-g -O2 -Wall". Let's say
you wanted to build the whole tree with debugging and warnings but
without optimization. You could do that with

  $ make clean; make CFLAGS=-g -Wall

or just in a particular subdir and everything below it

  $ cd devel; make clean; make CFLAGS=-g -Wall

Or maybe you want to build the scheduler using electricfence

  $ cd scheduler; make clean; make CFLAGS=-lefence


Installation location:
All installation locations are controlled by variables as well. Let's say
you wanted to do a test install into a directory pseudoroot rather than
on the system itself

  $ make; mkdir /tmp/foo; make DESTDIR=/tmp/foo install

If you are someone packaging for a distribution you want to use the paths
defined by the FHS for distributions, and install to a pseudoroot so you
can run your packaging results.

 $ make SYSCONFDIR=/etc; mkdir /tmp/foo; make DESTDIR=/tmp/foo \
        PREFIX=/usr SYSCONFDIR=/etc LOCALSTATEDIR=/var/

Note: SYSCONFDIR needs to be set during the build as the location of the
configuration files is embedded in the binaries. But none of the other
variables should need to be set at build time, only at install time.


File Preprocessing:
The build tree has a lightweight file preprocessing system. This is
mainly used to load values from Makefile.conf into text or scripts that
will be installed on the system. To use it:

1.) Name your input file with the name you want the output file to be
with a ".in" on the end. So if you wanted the post-processed file to
be "example" then name the input file "example.in".
2.) Edit your input file and wherever you would like to use a variable
insert "{$VARIABLE}" in the input file. While we mostly use this for
variables, you can actually use perl code if you need to.
3.) Insert the following into your Makefile (for our "example" above),
making sure to add something to your clean target to remove the
generated file

========================================================================
# include the preprocessing stuff
include $(TOP)/Makefile.process
# generate the example file
example: example-process

clean:
	rm -f example
========================================================================

Now you can also make other targets (like "all") dependent on the
"example" target.
