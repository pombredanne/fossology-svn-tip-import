#!/usr/bin/php
<?php
/***********************************************************
 Copyright (C) 2008 Hewlett-Packard Development Company, L.P.

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 version 2 as published by the Free Software Foundation.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
***********************************************************/
/*
 * Runner script to run Page level tests
 */
// set the path for where simpletest is
$path = '/usr/share/php' . PATH_SEPARATOR;
set_include_path(get_include_path() . PATH_SEPARATOR . $path);

/* simpletest includes */
require_once '/usr/local/simpletest/unit_tester.php';
require_once '/usr/local/simpletest/web_tester.php';
require_once '/usr/local/simpletest/reporter.php';

require_once ('../../../tests/TestEnvironment.php');
require_once('../../../tests/testClasses/timer.php');

$start = new timer();
print "Startnig Page Functional Tests at: " . date('r') . "\n";
$test = &new TestSuite('Fossology Repo UI Page Functional tests');

$test->addTestFile('UploadFileTest.php');
$test->addTestFile('UploadUrlTest.php');
$test->addTestFile('UploadSrvTest.php');
$test->addTestFile('OneShot-lgpl2.1.php');
$test->addTestFile('OneShot-lgpl2.1-T.php');
$test->addTestFile('CreateFolderTest.php');
$test->addTestFile('DeleteFolderTest.php');
$test->addTestFile('editFolderTest.php');
$test->addTestFile('editFolderNameOnlyTest.php');
$test->addTestFile('editFolderDescriptionOnlyTest.php');
$test->addTestFile('moveFolderTest.php');
//$test->addTestFile('browseUploadedTest.php');
$test->addTestFile('DupFolderTest.php');
$test->addTestFile('DupUploadTest.php');

if (TextReporter::inCli())
{
  $results = $test->run(new TextReporter()) ? 0 : 1;
  print "Ending Page Tests at: " . date('r') . "\n";
  $elapseTime = $start->TimeAgo($start->getStartTime());
  print "The Page Tests took {$elapseTime}to run\n";
  exit($results);
}
$test->run(new HtmlReporter());
$elapseTime = $start->TimeAgo($start->getStartTime());
print "The Page Functional Tests took {$elapseTime}to run\n";
?>