Skype End User License Agreement


IMPORTANT – PLEASE READ CAREFULLY:

INTERNATIONAL VERSIONS ARE AVAILABLE:

* IMPORTANT: version française sur (French version available at)
  http://www.skype.com/go/eula-fr
* IMPORTANT: 注意：简体中文《最终用户许可协议》在此浏览 (Simplified Chinese version
  available at) http://www.skype.com/go/eula-sc

Before reading the articles below, please take good notice of the following
preliminary terms, which terms make use of some of the definitions as specified
in Article 1 below:

Entering into this Agreement: This End User License Agreement constitutes a
valid and binding agreement between Skype Software S.A. and You, as a user, for
the use of the Skype Software. You must enter into this Agreement by clicking on
the ACCEPT button in order to be able to install and use the Skype Software.
Furthermore, by installing and (continuously) using the Skype Software You agree
to be bound by the terms of this Agreement and any new versions hereof.

Electronic Signatures and Agreement(s): You acknowledge and agree that by
clicking on the ACCEPT button or similar buttons or links as may be designated
by Skype to show Your approval of any foregoing texts and/or to download and
install the Skype Software, You are entering into a legal binding contract. You
hereby agree to the use of electronic communication in order to enter into
contracts, place orders and other records and to the electronic delivery of
notices, policies and records of transactions initiated or completed through the
Skype Software. Furthermore, You hereby waive any rights or requirements under
any laws or regulations in any jurisdiction which require an original
(non-electronic) signature or delivery or retention of non-electronic records,
to the extent permitted under applicable mandatory law.

No Emergency Calls: by entering into this Agreement You acknowledge and agree
that the Skype Software does not and does not intend to support or carry
emergency calls. Please also see article 7 below.

Jurisdiction’s Restrictions: if You are residing in a jurisdiction which
restricts the use of internet-based applications according to age, or which
restricts the ability to enter into agreements such as this agreement according
to age and You are under such a jurisdiction and under such age limit, You may
not enter into this Agreement and download, install or use the Skype Software.
Furthermore, if You are residing in a jurisdiction where it is forbidden by law
to offer or use software for internet telephony, You may not enter into this
Agreement and You may not download, install or use the Skype Software. By
entering into this Agreement You explicitly state that You have verified in Your
own jurisdiction if Your use of the Skype Software is allowed.


Article 1. Definitions


In this Agreement the following capitalized definitions are being used,
singular as well as plural.

1.1 Affiliate: any corporation, company or other entity that directly or
indirectly controls, is controlled by, or is under common control with, Skype.
For the purpose of this definition, the word "control" shall mean the direct or
indirect ownership of more than fifty percent (50%) of the outstanding voting
stock of the corporation, company, or other entity.

1.2 Agreement: this End User License Agreement, as may be renewed, modified
and/or amended from time to time.

1.3 API: application program interface that is included in or linked to the
Skype Software.

1.4 Documentation: any online or otherwise enclosed documentation provided by
Skype.

1.5 Effective Date: the date on which this Agreement is entered into by clicking
on the ACCEPT button as stated above or by downloading, installing and
(continuously) using the Skype Software.

1.6 IP Rights: any and all intellectual property rights, including but not
limited to copyrights, trademarks and patents, as well as know how and trade
secrets contained in or relating to the Skype Software, the Documentation, the
Skype Website or the Skype Promotional Materials.

1.7 Skype: the company established under the laws of Luxembourg, Skype Software
S.A.

1.8 Skype Promotional Materials: any and all names, signs, logos, banners and
any other materials, in whatever form, owned and/or used by Skype for the
promotion of its company, its products and activities.

1.9 Skype Software: the software distributed by Skype for internet telephony
applications, including without limitation the API, UI and Documentation, as
well as any future programming fixes, updates and upgrades thereof. 

1.10 Skype Staff: the officers, directors, employees and agents of Skype or its
Affiliates, or any other persons hired by Skype or its Affiliates in relation
with the execution of this Agreement. 

1.11 Skype Website: any and all elements and contents of the website available –
among other URL’s- under the URL www.skype.com, from which website the Skype
Software can be downloaded.

1.12 UI: the user interface of the Skype Software.

1.13 You: you, the end user of the Skype Software, also used in the form “Your”
where applicable.


Article 2. License and Restrictions


2.1 License. Subject to the terms of this Agreement, Skype hereby grants You a
limited, personal, non-commercial (at home or at work), non-exclusive,
non-sublicensable, non-assignable, free of charge license to download, install
and use the Skype Software on Your computer or PDA, for the sole purpose of
internet telephony applications and any other applications that may be
explicitly provided by Skype.

2.2 No Granting of Rights to Third Parties. You will not sell, assign, rent,
lease, distribute, export, import, act as an intermediary or provider, or
otherwise grant rights to third parties with regard to the Skype Software.

2.3 No Modifications. You will not undertake, cause, permit or authorize the
modification, creation of derivative works, translation, reverse engineering,
decompiling, disassembling or hacking of the Skype Software.

2.4 Third Parties. You acknowledge and agree that the Skype Software may be
incorporated into, and may incorporate itself, software and other technology
owned and controlled by third parties. Skype emphasizes that it will only
incorporate such third party software or technology for the purpose of (a)
adding new or additional functionality or (b) improving the technical
performance of the Skype Software. Any such third party software or technology
that is incorporated in the Skype Software falls under the scope of this
Agreement. Any and all other third party software or technology that may be
distributed together with the Skype Software will be subject to you explicitly
accepting a license agreement with that third party. You acknowledge and agree
that you will not enter into a contractual relationship with Skype or its
Affiliates regarding such third party software or technology and you will look
solely to the applicable third party and not to Skype or its Affiliates to
enforce any of your rights. 

2.5 New Versions of the Skype Software. Skype, in its sole discretion, reserves
the right to add additional features or functions, or to provide programming
fixes, updates and upgrades, to the Skype Software. You acknowledge and agree
that Skype has no obligation to make available to You any subsequent versions of
the Skype Software. You also agree that you may have to enter into a renewed
version of this Agreement, in the event you want to download, install or use a
new version of the Skype Software. Furthermore, you acknowledge and agree that
Skype, in its sole discretion, may modify or discontinue or suspend Your ability
to use any version of the Skype Software, or terminate any license hereunder, at
any time. Skype also may suspend or terminate any license hereunder and disable
any Skype Software You may already have accessed or installed without prior
notice at any time.

2.6 Paid Services. This Agreement applies to downloading, installing and using
the Skype Software, free of charge. The use of any paid services which may be
offered by Skype or its Affiliates, is subject to the additional “Terms of
Service” that are published on the Skype Website.


Article 3. Exceptions to License Restrictions


3.1 Redistribution. You are not allowed to redistribute the Skype Software,
unless You have agreed to and meet with the Distribution Terms that are
published on the Skype Website at http://www.skype.com/company/legal/promote/

3.2 API. You are not allowed to use or modify the API, unless You agree to and
meet with the following subsequent terms:

3.2.1 You may only make use of and/or modify the API to distribute the Skype
Software (a) for any legitimate purposes and (b) provided that You will not
remove, overtake, hide or otherwise make the UI inaccessible for end users.

3.2.2 You will constantly monitor the Skype Website in order to ensure that You
are distributing the latest stable version of the Skype Software as well as that
You are aware of any changes in the applicable legal documents. In the event You
cannot agree on any changes in any applicable legal document, You will
immediately cease any and all use of the API and, where applicable, any and all
use of the Skype Software.

3.2.3 You acknowledge and agree that Your use and/or modification of the API
will be at Your own risk and account.

3.3 You acknowledge and agree that any IP Rights arising directly from the API
are the exclusive ownership of Skype or its licensors without any compensation
to You. Insofar as necessary, this Agreement serves as a deed of assignment of
all of Your right, title and interest in and to such API modifications to Skype,
notwithstanding Your obligation to cooperate with Skype in order finalize any
other deed upon Skype’s first request. You hereby irrevocably waive to the
extent permitted by law any moral rights relating to YourAPI modifications. You
furthermore represent and warrant that (a) You are authorized to assign Your
rights as stated above and (b) Your API modifications are correct and accurate
and (c) the API modifications do not infringe upon any third parties’ rights,
including but not limited to intellectual property rights.

3.4 Any other Exceptions. If You are interested in doing anything else than
permitted under this Agreement or by the Distribution Terms, the API Terms or
the Skype Promotional Materials Terms, You will have to obtain Skype’s written
consent and agree on any further (commercial) terms. 


Article 4. Permission to Utilize


4.1 Permission to utilize Your computer. In order to receive the benefits
provided by the Skype Software, you hereby grant permission for the Skype
Software to utilize the processor and bandwidth of Your computer for the limited
purpose of facilitating the communication between You and other Skype Software
users. 

4.2 Protection of Your computer (resources). You understand that the Skype
Software will use its commercially reasonable efforts to protect the privacy and
integrity of Your computer resources and Your communication, however, You
acknowledge and agree that Skype cannot give any warranties in this respect. 


Article 5. Confidentiality and Privacy


5.1 Skype’s Confidential Information. You agree to take all reasonable steps at
all times to protect and maintain any confidential information regarding Skype,
its Affiliates, the Skype Staff, the Skype Software and the IP Rights, strictly
confidential.

5.2 Your Confidential Information and Your Privacy. Skype is committed to
respecting Your privacy and the confidentiality of Your personal data. The
“Privacy Policy” that is published on the Skype Website applies to the use of
Your personal data, the traffic data as well as the content contained in Your
communication(s). 


Article 6. IP Rights and Translations


6.1 Exclusive Ownership. You acknowledge and agree that any and all IP Rights
are and shall remain the exclusive property of Skype and its licensors. Nothing
in this Agreement intends to transfer any IP Rights to, or to vest any IP Rights
in, You. You are only entitled to the limited use of the IP Rights granted to
You in this Agreement. You will not take any action to jeopardize, limit or
interfere with the IP Rights. You acknowledge and agree that any unauthorized
use of the IP Rights is a violation of this Agreement as well as a violation of
intellectual property laws, including without limitation copyright laws and
trademark laws.

6.2 No Removal of Notices. You agree that You will not remove, obscure, make
illegible or alter any notices or indications of the IP Rights and/or Skype’s
rights and ownership thereof.

6.3 Translations. You acknowledge and agree that the intellectual property
rights regarding any translations made by You of any information on or
accessible through the Skype Website or as otherwise requested of You by Skype
at any time prior to or subsequent of the Effective Date will be and remain the
sole and exclusive property of Skype without any compensation to You. Insofar as
necessary, this Agreement serves as a deed of assignment of all of Your right,
title and interest in and to such translations to Skype, notwithstanding Your
obligation to cooperate with Skype in order finalize any other deed upon Skype’s
first request. You hereby irrevocably waive to the extent permitted by law any
moral rights relating to Your translations. You furthermore represent and
warrant that (a) You are authorized to assign Your rights as stated above and
(b) Your translations are correct and accurate and (c) the translations do not
infringe upon any third parties’ rights, including but not limited to
intellectual property rights.

6.4 Use of Skype Promotional Materials. You are not allowed to use the Skype
Promotional Materials, unless You have agreed on and meet with the Skype
Promotional Materials Terms as published on the Skype Website at
http://www.skype.com/company/legal/promote/


Article 7. Communication and Your Use of the Skype Software


7.1 Communication. Installing Skype Software enables You to communicate with
other Skype Software users. 

7.2 No Warranties. Skype cannot guarantee that You will always be able to
communicate with other Skype Software users, nor can Skype guarantee that You
can communicate without disruptions, delays or other communication-related
flaws. Skype will not be liable for any such disruptions, delays or other
omissions in any communication experienced when using Skype Software. 

7.3 No Control. You acknowledge and understand that Skype does not control, or
have any knowledge of, the content of any communication(s) spread by the use of
the Skype Software. The content of the communication is entirely the
responsibility of the person from whom such content originated. You, therefore,
may be exposed to content that is offensive, indecent or otherwise
objectionable. Skype will not be liable for any type of communication spread by
means of the Skype Software. 

7.4 No Emergency Services. You expressly agree and understand that the Skype
Software is not intended to support or carry emergency calls to any type of
hospital, law enforcement agency, medical care unit or any other kind of
emergency service. Skype, its Affiliates or Skype Staff are in no way liable for
such emergency calls.

7.5 Lawful purposes. You acknowledge and agree to use the Skype Software solely
for lawful purposes. In this respect You may not, without limitation (a)
intercept or monitor, damage or modify any communication which is not intended
for You, (b) use any type of spider, virus, worm, trojan-horse, time bomb or any
other codes or instructions that are designed to distort, delete, damage or
disassemble the Skype Software or the communication, or (c) send any unsolicited
commercial communication not permitted by applicable law. 


Article 8. Term and (Consequences of) Termination


8.1 Term. This Agreement will be effective as of the Effective Date and will
remain effective until terminated by either Skype or You as set forth below.

8.2 Termination by Skype. Skype may terminate this Agreement at any time, with
or without cause, by providing notice to You and/or by preventing Your access to
the Skype Software, as set forth in Article 2.5 above.

8.3 Termination by You. You may terminate this Agreement at any time, with or
without cause, provided that You will meet with the conditions as set forth in
Article 8.4 below.

8.4 Consequences of Termination. Upon termination of this Agreement, You (a)
acknowledge and agree that all licenses and rights to use the Skype Software
shall terminate, and (b) will cease any and all use of the Skype Software, and
(c) will remove the Skype Software from all hard drives, networks and other
storage media and destroy all copies of the Skype Software in Your possession or
under Your control.


Article 9. Your Representations and Warranties; Indemnification of Skype


9.1 Representations. You represent and warrant that You are authorized to enter
into this Agreement and comply with its terms. Furthermore, You represent and
warrant that You will at any and all times meet with Your obligations hereunder,
as well as any and all laws, regulations and policies that may apply to the use
of the Skype Software.

9.2 Indemnification. You agree to indemnify, defend and hold Skype, its
Affiliates and the Skype Staff harmless from and against any and all liability
and costs, including reasonable attorneys’ fees incurred by said parties, in
connection with or arising out of Your (a) violation or breach of any term of
this Agreement or any applicable law, regulation, policy or guideline, whether
or not referenced herein, or (b) violation of any rights of any third party, or
(c) use or misuse of the Skype Software, or (d) use and/or modification of the
API or (e) communication spread by means of the Skype Software.


Article 10 Disclaimer of Warranties


10.1 No warranties. THE SKYPE SOFTWARE IS PROVIDED “AS IS”, WITH NO WARRANTIES
WHATSOEVER; SKYPE DOES NOT, EITHER EXPRESSED, IMPLIED OR STATUTORY, MAKE ANY
WARRANTIES, CLAIMS OR REPRESENTATIONS WITH RESPECT TO THE SKYPE SOFTWARE,
INCLUDING, WITHOUT LIMITATION, WARRANTIES OF QUALITY, PERFORMANCE,
NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR USE OR A PARTICULAR PURPOSE.
SKYPE FURTHER DOES NOT REPRESENT OR WARRANT THAT THE SKYPE SOFTWARE WILL ALWAYS
BE AVAILABLE, ACCESSIBLE, UNINTERRUPTED, TIMELY, SECURE, ACCURATE, COMPLETE, AND
ERROR-FREE OR WILL OPERATE WITHOUT PACKET LOSS, NOR DOES SKYPE WARRANT ANY
CONNECTION TO OR TRANSMISSION FROM THE INTERNET, OR ANY QUALITY OF CALLS MADE
THROUGH THE SKYPE SOFTWARE.

10.2 Your own Risk. You acknowledge and agree that the entire risk arising out
of the use or performance of the Skype Software remains with You, to the maximum
extent permitted by law.

10.3 Jurisdiction’s Limitations. As some jurisdictions do not allow some of the
exclusions or limitations as set forth above, some of these exclusions or
limitations may not apply to You.


Article 11. Limitation of Liability


11.1 No Liability. The Skype Software is being provided to You free of charge.
ACCORDINGLY, YOU ACKNOWLEDGE AND AGREE THAT SKYPE, ITS AFFILIATES AND THE SKYPE
STAFF WILL HAVE NO LIABILITY IN CONNECTION WITH OR ARISING FROM YOUR USE OF THE
SKYPE SOFTWARE, AS SET FORTH BELOW.

11.2 Limitation of Liability. IN NO EVENT SHALL SKYPE, ITS AFFILIATES OR THE
SKYPE STAFF BE LIABLE, WHETHER IN CONTRACT, WARRANTY, TORT (INCLUDING
NEGLIGENCE), PRODUCT LIABILITY OR ANY OTHER FORM OF LIABILITY, FOR ANY INDIRECT,
INCIDENTAL, SPECIAL OR CONSEQUENTIAL DAMAGES (INCLUDING WITHOUT LIMITATION ANY
LOSS OF DATA, INTERRUPTION, COMPUTER FAILURE OR PECUNIARY LOSS) ARISING OUT OF
THE USE OR INABILITY TO USE THE SKYPE SOFTWARE, EVEN IF SKYPE, ITS AFFILIATES OR
THE SKYPE STAFF HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

11.3 Remedy. YOUR ONLY RIGHT OR REMEDY WITH RESPECT TO ANY PROBLEMS OR
DISSATISFACTION WITH THE SKYPE SOFTWARE IS TO DEINSTALL AND CEASE USE OF SUCH
SKYPE SOFTWARE.

11.4 Jurisdiction’s Limitations. As some jurisdictions do not allow some of the
exclusions or limitations as set forth above, some of these exclusions or
limitations may not apply to You.


Article 12. General Provisions


12.1 New versions of the Agreement. Skype reserves the right to modify this
Agreement at any time by providing such revised Agreement to You or by
publishing the revised Agreement on the Skype Website. Your continued use of the
Skype Software shall constitute Your acceptance to be bound by the terms and
conditions of the revised Agreement.

12.2 Entire Agreement. The terms and conditions of this Agreement constitute the
entire agreement between You and Skype with respect to the subject matter hereof
and will supersede and replace all prior understandings and agreements, in
whatever form, regarding the subject matter.

12.3 Partial Invalidity. Should any term or provision hereof be deemed invalid,
void or enforceable either in its entirety or in a particular application, the
remainder of this Agreement shall nonetheless remain in full force and effect.

12.4 No waiver. The failure of Skype at any time or times to require performance
of any provisions hereof shall in no manner affect its right at a later time to
enforce the same unless the same is explicitly waived in writing and signed by
Skype.

12.5 No Assignment by You. You are not allowed to assign this Agreement or any
rights hereunder.

12.6 Assignment by Skype. Skype is allowed to at its sole discretion assign this
Agreement or any rights hereunder to any Affiliate, without giving prior notice.

12.7 Applicable Law. This Agreement shall be governed by and construed in
accordance with the laws of Luxembourg without giving effect to the conflict of
laws or provisions of Luxembourg or Your actual state or country of residence.

12.8 Competent Court. Any legal proceedings arising out of or relating to this
Agreement will be subject to the exclusive jurisdiction of any court of
Luxembourg sitting in Luxembourg.


YOU EXPRESSLY ACKNOWLEDGE THAT YOU HAVE READ THIS AGREEMENT AND UNDERSTAND THE
RIGHTS, OBLIGATIONS, TERMS AND CONDITIONS SET FORTH HEREIN. BY CLICKING ON THE
ACCEPT BUTTON AND/OR CONTINUING TO INSTALL THE SKYPE SOFTWARE, YOU EXPRESSLY
CONSENT TO BE BOUND BY ITS TERMS AND CONSITIONS AND GRANT TO SKYPE THE RIGHTS
SET FORTH HEREIN.


Last revised: October 2004.