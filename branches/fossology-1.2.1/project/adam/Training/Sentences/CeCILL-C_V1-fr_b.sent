
<sentence>CONTRAT DE LICENCE DE LOGICIEL LIBRE CeCILL-C


    </sentence><sentence>Avertissement

</sentence><sentence>Ce contrat est une licence de logiciel libre issue d'une concertation
entre ses auteurs afin que le respect de deux grands principes pr�side �
sa r�daction:

    </sentence><sentence>* d'une part, le respect des principes de diffusion des logiciels
      libres: acc�s au code source, droits �tendus conf�r�s aux
      utilisateurs,
    </sentence><sentence>* d'autre part, la d�signation d'un droit applicable, le droit
      fran�ais, auquel elle est conforme, tant au regard du droit de la
      responsabilit� civile que du droit de la propri�t� intellectuelle
      et de la protection qu'il offre aux auteurs et titulaires des
      droits patrimoniaux sur un logiciel.

</sentence><sentence>Les auteurs de la licence CeCILL-C (pour Ce[a] C[nrs] I[nria] L[ogiciel]
L[ibre]) sont:

</sentence><sentence>Commissariat � l'Energie Atomique - CEA, �tablissement public de
recherche � caract�re scientifique, technique et industriel, dont le
si�ge est situ� 25 rue Leblanc, immeuble Le Ponant D, 75015 Paris.

</sentence><sentence>Centre National de la Recherche Scientifique - CNRS, �tablissement
public � caract�re scientifique et technologique, dont le si�ge est
situ� 3 rue Michel-Ange, 75794 Paris cedex 16.

</sentence><sentence>Institut National de Recherche en Informatique et en Automatique -
INRIA, �tablissement public � caract�re scientifique et technologique,
dont le si�ge est situ� Domaine de Voluceau, Rocquencourt, BP 105, 78153
Le Chesnay cedex.


    </sentence><sentence>Pr�ambule

</sentence><sentence>Ce contrat est une licence de logiciel libre dont l'objectif est de
conf�rer aux utilisateurs la libert� de modifier et de r�utiliser le
logiciel r�gi par cette licence.

</sentence><sentence>L'exercice de cette libert� est assorti d'une obligation de remettre �
la disposition de la communaut� les modifications apport�es au code
source du logiciel afin de contribuer � son �volution.

</sentence><sentence>L'accessibilit� au code source et les droits de copie, de modification
et de redistribution qui d�coulent de ce contrat ont pour contrepartie
de n'offrir aux utilisateurs qu'une garantie limit�e et de ne faire
peser sur l'auteur du logiciel, le titulaire des droits patrimoniaux et
les conc�dants successifs qu'une responsabilit� restreinte.

</sentence><sentence>A cet �gard l'attention de l'utilisateur est attir�e sur les risques
associ�s au chargement, � l'utilisation, � la modification et/ou au
d�veloppement et � la reproduction du logiciel par l'utilisateur �tant
donn� sa sp�cificit� de logiciel libre, qui peut le rendre complexe �
manipuler et qui le r�serve donc � des d�veloppeurs ou des
professionnels avertis poss�dant des connaissances informatiques
approfondies.</sentence><sentence> Les utilisateurs sont donc invit�s � charger et tester
l'ad�quation du logiciel � leurs besoins dans des conditions permettant
d'assurer la s�curit� de leurs syst�mes et/ou de leurs donn�es et, plus
g�n�ralement, � l'utiliser et l'exploiter dans les m�mes conditions de
s�curit�.</sentence><sentence> Ce contrat peut �tre reproduit et diffus� librement, sous
r�serve de le conserver en l'�tat, sans ajout ni suppression de clauses.

</sentence><sentence>Ce contrat est susceptible de s'appliquer � tout logiciel dont le
titulaire des droits patrimoniaux d�cide de soumettre l'exploitation aux
dispositions qu'il contient.


    </sentence><sentence>Article 1 - DEFINITIONS

</sentence><sentence>Dans ce contrat, les termes suivants, lorsqu'ils seront �crits avec une
lettre capitale, auront la signification suivante:

</sentence><sentence>Contrat: d�signe le pr�sent contrat de licence, ses �ventuelles versions
post�rieures et annexes.

</sentence><sentence>Logiciel: d�signe le logiciel sous sa forme de Code Objet et/ou de Code
Source et le cas �ch�ant sa documentation, dans leur �tat au moment de
l'acceptation du Contrat par le Licenci�.

</sentence><sentence>Logiciel Initial: d�signe le Logiciel sous sa forme de Code Source et
�ventuellement de Code Objet et le cas �ch�ant sa documentation, dans
leur �tat au moment de leur premi�re diffusion sous les termes du Contrat.

</sentence><sentence>Logiciel Modifi�: d�signe le Logiciel modifi� par au moins une
Contribution Int�gr�e.

</sentence><sentence>Code Source: d�signe l'ensemble des instructions et des lignes de
programme du Logiciel et auquel l'acc�s est n�cessaire en vue de
modifier le Logiciel.

</sentence><sentence>Code Objet: d�signe les fichiers binaires issus de la compilation du
Code Source.

</sentence><sentence>Titulaire: d�signe le ou les d�tenteurs des droits patrimoniaux d'auteur
sur le Logiciel Initial.

</sentence><sentence>Licenci�: d�signe le ou les utilisateurs du Logiciel ayant accept� le
Contrat.

</sentence><sentence>Contributeur: d�signe le Licenci� auteur d'au moins une Contribution
Int�gr�e.

</sentence><sentence>Conc�dant: d�signe le Titulaire ou toute personne physique ou morale
distribuant le Logiciel sous le Contrat.

</sentence><sentence>Contribution Int�gr�e: d�signe l'ensemble des modifications,
corrections, traductions, adaptations et/ou nouvelles fonctionnalit�s
int�gr�es dans le Code Source par tout Contributeur.

</sentence><sentence>Module Li�: d�signe un ensemble de fichiers sources y compris leur
documentation qui, sans modification du Code Source, permet de r�aliser
des fonctionnalit�s ou services suppl�mentaires � ceux fournis par le
Logiciel.

</sentence><sentence>Logiciel D�riv�: d�signe toute combinaison du Logiciel, modifi� ou non,
et d'un Module Li�.

</sentence><sentence>Parties: d�signe collectivement le Licenci� et le Conc�dant.

</sentence><sentence>Ces termes s'entendent au singulier comme au pluriel.


    </sentence><sentence>Article 2 - OBJET

</sentence><sentence>Le Contrat a pour objet la concession par le Conc�dant au Licenci� d'une
licence non exclusive, cessible et mondiale du Logiciel telle que
d�finie ci-apr�s � l'article 5 pour toute la dur�e de protection des droits
portant sur ce Logiciel.


    </sentence><sentence>Article 3 - ACCEPTATION

</sentence><sentence>3.1 L'acceptation par le Licenci� des termes du Contrat est r�put�e
acquise du fait du premier des faits suivants:

    </sentence><sentence>* (i) le chargement du Logiciel par tout moyen notamment par
      t�l�chargement � partir d'un serveur distant ou par chargement �
      partir d'un support physique;
    </sentence><sentence>* (ii) le premier exercice par le Licenci� de l'un quelconque des
      droits conc�d�s par le Contrat.

</sentence><sentence>3.2 Un exemplaire du Contrat, contenant notamment un avertissement
relatif aux sp�cificit�s du Logiciel, � la restriction de garantie et �
la limitation � un usage par des utilisateurs exp�riment�s a �t� mis �
disposition du Licenci� pr�alablement � son acceptation telle que
d�finie � l'article 3.1 ci dessus et le Licenci� reconna�t en avoir pris
connaissance.


    </sentence><sentence>Article 4 - ENTREE EN VIGUEUR ET DUREE


      </sentence><sentence>4.1 ENTREE EN VIGUEUR

</sentence><sentence>Le Contrat entre en vigueur � la date de son acceptation par le Licenci�
telle que d�finie en 3.1.


      </sentence><sentence>4.2 DUREE

</sentence><sentence>Le Contrat produira ses effets pendant toute la dur�e l�gale de
protection des droits patrimoniaux portant sur le Logiciel.


    </sentence><sentence>Article 5 - ETENDUE DES DROITS CONCEDES

</sentence><sentence>Le Conc�dant conc�de au Licenci�, qui accepte, les droits suivants sur
le Logiciel pour toutes destinations et pour la dur�e du Contrat dans
les conditions ci-apr�s d�taill�es.

</sentence><sentence>Par ailleurs, si le Conc�dant d�tient ou venait � d�tenir un ou
plusieurs brevets d'invention prot�geant tout ou partie des
fonctionnalit�s du Logiciel ou de ses composants, il s'engage � ne pas
opposer les �ventuels droits conf�r�s par ces brevets aux Licenci�s
successifs qui utiliseraient, exploiteraient ou modifieraient le
Logiciel.</sentence><sentence> En cas de cession de ces brevets, le Conc�dant s'engage �
faire reprendre les obligations du pr�sent alin�a aux cessionnaires.


      </sentence><sentence>5.1 DROIT D'UTILISATION

</sentence><sentence>Le Licenci� est autoris� � utiliser le Logiciel, sans restriction quant
aux domaines d'application, �tant ci-apr�s pr�cis� que cela comporte:

   </sentence><sentence>1. </sentence><sentence>la reproduction permanente ou provisoire du Logiciel en tout ou
      partie par tout moyen et sous toute forme.

   </sentence><sentence>2. </sentence><sentence>le chargement, l'affichage, l'ex�cution, ou le stockage du
      Logiciel sur tout support.

   </sentence><sentence>3. </sentence><sentence>la possibilit� d'en observer, d'en �tudier, ou d'en tester le
      fonctionnement afin de d�terminer les id�es et principes qui sont
      � la base de n'importe quel �l�ment de ce Logiciel;</sentence><sentence> et ceci,
      lorsque le Licenci� effectue toute op�ration de chargement,
      d'affichage, d'ex�cution, de transmission ou de stockage du
      Logiciel qu'il est en droit d'effectuer en vertu du Contrat.


      </sentence><sentence>5.2 DROIT DE MODIFICATION

</sentence><sentence>Le droit de modification comporte le droit de traduire, d'adapter,
d'arranger ou d'apporter toute autre modification au Logiciel et le
droit de reproduire le logiciel en r�sultant.</sentence><sentence> Il comprend en particulier
le droit de cr�er un Logiciel D�riv�.

</sentence><sentence>Le Licenci� est autoris� � apporter toute modification au Logiciel sous
r�serve de mentionner, de fa�on explicite, son nom en tant qu'auteur de
cette modification et la date de cr�ation de celle-ci.


      </sentence><sentence>5.3 DROIT DE DISTRIBUTION

</sentence><sentence>Le droit de distribution comporte notamment le droit de diffuser, de
transmettre et de communiquer le Logiciel au public sur tout support et
par tout moyen ainsi que le droit de mettre sur le march� � titre
on�reux ou gratuit, un ou des exemplaires du Logiciel par tout proc�d�.

</sentence><sentence>Le Licenci� est autoris� � distribuer des copies du Logiciel, modifi� ou
non, � des tiers dans les conditions ci-apr�s d�taill�es.


        </sentence><sentence>5.3.1 DISTRIBUTION DU LOGICIEL SANS MODIFICATION

</sentence><sentence>Le Licenci� est autoris� � distribuer des copies conformes du Logiciel,
sous forme de Code Source ou de Code Objet, � condition que cette
distribution respecte les dispositions du Contrat dans leur totalit� et
soit accompagn�e:

   </sentence><sentence>1. </sentence><sentence>d'un exemplaire du Contrat,

   </sentence><sentence>2. </sentence><sentence>d'un avertissement relatif � la restriction de garantie et de
      responsabilit� du Conc�dant telle que pr�vue aux articles 8
      et 9,

et que, dans le cas o� seul le Code Objet du Logiciel est redistribu�,
le Licenci� permette un acc�s effectif au Code Source complet du
Logiciel pendant au moins toute la dur�e de sa distribution du Logiciel,
�tant entendu que le co�t additionnel d'acquisition du Code Source ne
devra pas exc�der le simple co�t de transfert des donn�es.


        </sentence><sentence>5.3.2 DISTRIBUTION DU LOGICIEL MODIFIE

</sentence><sentence>Lorsque le Licenci� apporte une Contribution Int�gr�e au Logiciel, les
conditions de distribution du Logiciel Modifi� en r�sultant sont alors
soumises � l'int�gralit� des dispositions du Contrat.

</sentence><sentence>Le Licenci� est autoris� � distribuer le Logiciel Modifi� sous forme de
code source ou de code objet, � condition que cette distribution
respecte les dispositions du Contrat dans leur totalit� et soit
accompagn�e:

   </sentence><sentence>1. </sentence><sentence>d'un exemplaire du Contrat,

   </sentence><sentence>2. </sentence><sentence>d'un avertissement relatif � la restriction de garantie et de
      responsabilit� du Conc�dant telle que pr�vue aux articles 8
      et 9,

et que, dans le cas o� seul le code objet du Logiciel Modifi� est
redistribu�, le Licenci� permette un acc�s effectif � son code source
complet pendant au moins toute la dur�e de sa distribution du Logiciel
Modifi�, �tant entendu que le co�t additionnel d'acquisition du code
source ne devra pas exc�der le simple co�t de transfert des donn�es.


        </sentence><sentence>5.3.3 DISTRIBUTION DU LOGICIEL DERIVE

</sentence><sentence>Lorsque le Licenci� cr�e un Logiciel D�riv�, ce Logiciel D�riv� peut
�tre distribu� sous un contrat de licence autre que le pr�sent Contrat �
condition de respecter les obligations de mention des droits sur le
Logiciel telles que d�finies � l'article 6.4. </sentence><sentence>Dans le cas o� la cr�ation du
Logiciel D�riv� a n�cessit� une modification du Code Source le licenci�
s'engage � ce que: 

   </sentence><sentence>1. </sentence><sentence>le Logiciel Modifi� correspondant � cette modification soit r�gi
      par le pr�sent Contrat,
   </sentence><sentence>2. </sentence><sentence>les Contributions Int�gr�es dont le Logiciel Modifi� r�sulte
      soient clairement identifi�es et document�es,
   </sentence><sentence>3. </sentence><sentence>le Licenci� permette un acc�s effectif au code source du Logiciel
      Modifi�, pendant au moins toute la dur�e de la distribution du
      Logiciel D�riv�, de telle sorte que ces modifications puissent
      �tre reprises dans une version ult�rieure du Logiciel, �tant
      entendu que le co�t additionnel d'acquisition du code source du
      Logiciel Modifi� ne devra pas exc�der le simple co�t du transfert
      des donn�es.


        </sentence><sentence>5.3.4 COMPATIBILITE AVEC LA LICENCE CeCILL

</sentence><sentence>Lorsqu'un Logiciel Modifi� contient une Contribution Int�gr�e soumise au
contrat de licence CeCILL, ou lorsqu'un Logiciel D�riv� contient un
Module Li� soumis au contrat de licence CeCILL, les stipulations pr�vues
au troisi�me item de l'article 6.4 sont facultatives.


    </sentence><sentence>Article 6 - PROPRIETE INTELLECTUELLE


      </sentence><sentence>6.1 SUR LE LOGICIEL INITIAL

</sentence><sentence>Le Titulaire est d�tenteur des droits patrimoniaux sur le Logiciel
Initial.</sentence><sentence> Toute utilisation du Logiciel Initial est soumise au respect
des conditions dans lesquelles le Titulaire a choisi de diffuser son
oeuvre et nul autre n'a la facult� de modifier les conditions de
diffusion de ce Logiciel Initial.

</sentence><sentence>Le Titulaire s'engage � ce que le Logiciel Initial reste au moins r�gi
par le Contrat et ce, pour la dur�e vis�e � l'article 4.2.


      </sentence><sentence>6.2 SUR LES CONTRIBUTIONS INTEGREES

</sentence><sentence>Le Licenci� qui a d�velopp� une Contribution Int�gr�e est titulaire sur
celle-ci des droits de propri�t� intellectuelle dans les conditions
d�finies par la l�gislation applicable.


      </sentence><sentence>6.3 SUR LES MODULES LIES

</sentence><sentence>Le Licenci� qui a d�velopp� un Module Li� est titulaire sur celui-ci des
droits de propri�t� intellectuelle dans les conditions d�finies par la
l�gislation applicable et reste libre du choix du contrat r�gissant sa
diffusion dans les conditions d�finies � l'article 5.3.3.


      </sentence><sentence>6.4 MENTIONS DES DROITS

</sentence><sentence>Le Licenci� s'engage express�ment:

   </sentence><sentence>1. </sentence><sentence>� ne pas supprimer ou modifier de quelque mani�re que ce soit les
      mentions de propri�t� intellectuelle appos�es sur le Logiciel;

   </sentence><sentence>2. </sentence><sentence>� reproduire � l'identique lesdites mentions de propri�t�
      intellectuelle sur les copies du Logiciel modifi� ou non;

   </sentence><sentence>3. </sentence><sentence>� faire en sorte que l'utilisation du Logiciel, ses mentions de
      propri�t� intellectuelle et le fait qu'il est r�gi par le Contrat
      soient indiqu�s dans un texte facilement accessible notamment
      depuis l'interface de tout Logiciel D�riv�.

</sentence><sentence>Le Licenci� s'engage � ne pas porter atteinte, directement ou
indirectement, aux droits de propri�t� intellectuelle du Titulaire et/ou
des Contributeurs sur le Logiciel et � prendre, le cas �ch�ant, �
l'�gard de son personnel toutes les mesures n�cessaires pour assurer le
respect des dits droits de propri�t� intellectuelle du Titulaire et/ou
des Contributeurs.


    </sentence><sentence>Article 7 - SERVICES ASSOCIES

</sentence><sentence>7.1 Le Contrat n'oblige en aucun cas le Conc�dant � la r�alisation de
prestations d'assistance technique ou de maintenance du Logiciel.

</sentence><sentence>Cependant le Conc�dant reste libre de proposer ce type de services.</sentence><sentence> Les
termes et conditions d'une telle assistance technique et/ou d'une telle
maintenance seront alors d�termin�s dans un acte s�par�.</sentence><sentence> Ces actes de
maintenance et/ou assistance technique n'engageront que la seule
responsabilit� du Conc�dant qui les propose.

</sentence><sentence>7.2 De m�me, tout Conc�dant est libre de proposer, sous sa seule
responsabilit�, � ses licenci�s une garantie, qui n'engagera que lui,
lors de la redistribution du Logiciel et/ou du Logiciel Modifi� et ce,
dans les conditions qu'il souhaite.</sentence><sentence> Cette garantie et les modalit�s
financi�res de son application feront l'objet d'un acte s�par� entre le
Conc�dant et le Licenci�.


    </sentence><sentence>Article 8 - RESPONSABILITE

</sentence><sentence>8.1 Sous r�serve des dispositions de l'article 8.2, le Licenci� a la 
facult�, sous r�serve de prouver la faute du Conc�dant concern�, de
solliciter la r�paration du pr�judice direct qu'il subirait du fait du
Logiciel et dont il apportera la preuve.

</sentence><sentence>8.2 La responsabilit� du Conc�dant est limit�e aux engagements pris en
application du Contrat et ne saurait �tre engag�e en raison notamment:
</sentence><sentence>(i) des dommages dus � l'inex�cution, totale ou partielle, de ses
obligations par le Licenci�, </sentence><sentence>(ii) des dommages directs ou indirects
d�coulant de l'utilisation ou des performances du Logiciel subis par le
Licenci� et </sentence><sentence>(iii) plus g�n�ralement d'un quelconque dommage indirect.</sentence><sentence> En
particulier, les Parties conviennent express�ment que tout pr�judice
financier ou commercial (par exemple perte de donn�es, perte de
b�n�fices, perte d'exploitation, perte de client�le ou de commandes,
manque � gagner, trouble commercial quelconque) ou toute action dirig�e
contre le Licenci� par un tiers, constitue un dommage indirect et
n'ouvre pas droit � r�paration par le Conc�dant.


    </sentence><sentence>Article 9 - GARANTIE

</sentence><sentence>9.1 Le Licenci� reconna�t que l'�tat actuel des connaissances
scientifiques et techniques au moment de la mise en circulation du
Logiciel ne permet pas d'en tester et d'en v�rifier toutes les
utilisations ni de d�tecter l'existence d'�ventuels d�fauts.</sentence><sentence> L'attention
du Licenci� a �t� attir�e sur ce point sur les risques associ�s au
chargement, � l'utilisation, la modification et/ou au d�veloppement et �
la reproduction du Logiciel qui sont r�serv�s � des utilisateurs avertis.

</sentence><sentence>Il rel�ve de la responsabilit� du Licenci� de contr�ler, par tous
moyens, l'ad�quation du produit � ses besoins, son bon fonctionnement et
de s'assurer qu'il ne causera pas de dommages aux personnes et aux biens.

</sentence><sentence>9.2 Le Conc�dant d�clare de bonne foi �tre en droit de conc�der
l'ensemble des droits attach�s au Logiciel (comprenant notamment les
droits vis�s � l'article 5).

</sentence><sentence>9.3 Le Licenci� reconna�t que le Logiciel est fourni "en l'�tat" par le
Conc�dant sans autre garantie, expresse ou tacite, que celle pr�vue �
l'article 9.2 et notamment sans aucune garantie sur sa valeur commerciale,
son caract�re s�curis�, innovant ou pertinent.

</sentence><sentence>En particulier, le Conc�dant ne garantit pas que le Logiciel est exempt
d'erreur, qu'il fonctionnera sans interruption, qu'il sera compatible
avec l'�quipement du Licenci� et sa configuration logicielle ni qu'il
remplira les besoins du Licenci�.

</sentence><sentence>9.4 Le Conc�dant ne garantit pas, de mani�re expresse ou tacite, que le
Logiciel ne porte pas atteinte � un quelconque droit de propri�t�
intellectuelle d'un tiers portant sur un brevet, un logiciel ou sur tout
autre droit de propri�t�.</sentence><sentence> Ainsi, le Conc�dant exclut toute garantie au
profit du Licenci� contre les actions en contrefa�on qui pourraient �tre
diligent�es au titre de l'utilisation, de la modification, et de la
redistribution du Logiciel.</sentence><sentence> N�anmoins, si de telles actions sont
exerc�es contre le Licenci�, le Conc�dant lui apportera son aide
technique et juridique pour sa d�fense.</sentence><sentence> Cette aide technique et
juridique est d�termin�e au cas par cas entre le Conc�dant concern� et
le Licenci� dans le cadre d'un protocole d'accord.</sentence><sentence> Le Conc�dant d�gage
toute responsabilit� quant � l'utilisation de la d�nomination du
Logiciel par le Licenci�.</sentence><sentence> Aucune garantie n'est apport�e quant �
l'existence de droits ant�rieurs sur le nom du Logiciel et sur
l'existence d'une marque.


    </sentence><sentence>Article 10 - RESILIATION

</sentence><sentence>10.1 En cas de manquement par le Licenci� aux obligations mises � sa
charge par le Contrat, le Conc�dant pourra r�silier de plein droit le
Contrat trente (30) jours apr�s notification adress�e au Licenci� et
rest�e sans effet.

</sentence><sentence>10.2 Le Licenci� dont le Contrat est r�sili� n'est plus autoris� �
utiliser, modifier ou distribuer le Logiciel. </sentence><sentence>Cependant, toutes les
licences qu'il aura conc�d�es ant�rieurement � la r�siliation du Contrat
resteront valides sous r�serve qu'elles aient �t� effectu�es en
conformit� avec le Contrat.


    </sentence><sentence>Article 11 - DISPOSITIONS DIVERSES


      </sentence><sentence>11.1 CAUSE EXTERIEURE

</sentence><sentence>Aucune des Parties ne sera responsable d'un retard ou d'une d�faillance
d'ex�cution du Contrat qui serait d� � un cas de force majeure, un cas
fortuit ou une cause ext�rieure, telle que, notamment, le mauvais
fonctionnement ou les interruptions du r�seau �lectrique ou de
t�l�communication, la paralysie du r�seau li�e � une attaque
informatique, l'intervention des autorit�s gouvernementales, les
catastrophes naturelles, les d�g�ts des eaux, les tremblements de terre,
le feu, les explosions, les gr�ves et les conflits sociaux, l'�tat de
guerre...

</sentence><sentence>11.2 Le fait, par l'une ou l'autre des Parties, d'omettre en une ou
plusieurs occasions de se pr�valoir d'une ou plusieurs dispositions du
Contrat, ne pourra en aucun cas impliquer renonciation par la Partie
int�ress�e � s'en pr�valoir ult�rieurement.

</sentence><sentence>11.3 Le Contrat annule et remplace toute convention ant�rieure, �crite
ou orale, entre les Parties sur le m�me objet et constitue l'accord
entier entre les Parties sur cet objet.</sentence><sentence> Aucune addition ou modification
aux termes du Contrat n'aura d'effet � l'�gard des Parties � moins
d'�tre faite par �crit et sign�e par leurs repr�sentants d�ment habilit�s.

</sentence><sentence>11.4 Dans l'hypoth�se o� une ou plusieurs des dispositions du Contrat
s'av�rerait contraire � une loi ou � un texte applicable, existants ou
futurs, cette loi ou ce texte pr�vaudrait, et les Parties feraient les
amendements n�cessaires pour se conformer � cette loi ou � ce texte.
</sentence><sentence>Toutes les autres dispositions resteront en vigueur.</sentence><sentence> De m�me, la
nullit�, pour quelque raison que ce soit, d'une des dispositions du
Contrat ne saurait entra�ner la nullit� de l'ensemble du Contrat.


      </sentence><sentence>11.5 LANGUE

</sentence><sentence>Le Contrat est r�dig� en langue fran�aise et en langue anglaise, ces
deux versions faisant �galement foi.


    </sentence><sentence>Article 12 - NOUVELLES VERSIONS DU CONTRAT

</sentence><sentence>12.1 Toute personne est autoris�e � copier et distribuer des copies de
ce Contrat.

</sentence><sentence>12.2 Afin d'en pr�server la coh�rence, le texte du Contrat est prot�g�
et ne peut �tre modifi� que par les auteurs de la licence, lesquels se
r�servent le droit de publier p�riodiquement des mises � jour ou de
nouvelles versions du Contrat, qui poss�deront chacune un num�ro
distinct.</sentence><sentence> Ces versions ult�rieures seront susceptibles de prendre en
compte de nouvelles probl�matiques rencontr�es par les logiciels libres.

</sentence><sentence>12.3 Tout Logiciel diffus� sous une version donn�e du Contrat ne pourra
faire l'objet d'une diffusion ult�rieure que sous la m�me version du
Contrat ou une version post�rieure.


    </sentence><sentence>Article 13 - LOI APPLICABLE ET COMPETENCE TERRITORIALE

</sentence><sentence>13.1 Le Contrat est r�gi par la loi fran�aise.</sentence><sentence> Les Parties conviennent
de tenter de r�gler � l'amiable les diff�rends ou litiges qui
viendraient � se produire par suite ou � l'occasion du Contrat.

</sentence><sentence>13.2 A d�faut d'accord amiable dans un d�lai de deux (2) mois � compter
de leur survenance et sauf situation relevant d'une proc�dure d'urgence,
les diff�rends ou litiges seront port�s par la Partie la plus diligente
devant les Tribunaux comp�tents de Paris.


</sentence><sentence>Version 1.0 du 2006-09-05.</sentence>
