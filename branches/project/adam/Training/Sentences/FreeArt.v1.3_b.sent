<sentence>[ Copyleft Attitude ]

</sentence><sentence>Free Art License 1.3 (FAL 1.3)

</sentence><sentence>Preamble

</sentence><sentence>The Free Art License grants the right to freely copy, distribute, and transform creative works without infringing the author's rights.

</sentence><sentence>The Free Art License recognizes and protects these rights.</sentence><sentence> Their implementation has been reformulated in order to allow everyone to use creations of the human mind in a creative manner, regardless of their types and ways of expression.

</sentence><sentence>While the public's access to creations of the human mind usually is restricted by the implementation of copyright law, it is favoured by the Free Art License.</sentence><sentence> This license intends to allow the use of a work’s resources;</sentence><sentence> to establish new conditions for creating in order to increase creation opportunities.</sentence><sentence> The Free Art License grants the right to use a work, and acknowledges the right holder’s and the user’s rights and responsibility.

</sentence><sentence>The invention and development of digital technologies, Internet and Free Software have changed creation methods:</sentence><sentence> creations of the human mind can obviously be distributed, exchanged, and transformed.</sentence><sentence> They allow to produce common works to which everyone can contribute to the benefit of all.

</sentence><sentence>The main rationale for this Free Art License is to promote and protect these creations of the human mind according to the principles of copyleft:</sentence><sentence> freedom to use, copy, distribute, transform, and prohibition of exclusive appropriation.

</sentence><sentence>Definitions

</sentence><sentence>“work” either means the initial work, the subsequent works or the common work as defined hereafter:

</sentence><sentence>“common work” means a work composed of the initial work and all subsequent contributions to it (originals and copies).</sentence><sentence> The initial author is the one who, by choosing this license, defines the conditions under which contributions are made.

</sentence><sentence>“Initial work” means the work created by the initiator of the common work (as defined above), the copies of which can be modified by whoever wants to

“Subsequent works” means the contributions made by authors who participate in the evolution of the common work by exercising the rights to reproduce, distribute, and modify that are granted by the license.

</sentence><sentence>“Originals” (sources or resources of the work) means all copies of either the initial work or any subsequent work mentioning a date and used by their author(s) as references for any subsequent updates, interpretations, copies or reproductions.

</sentence><sentence>“Copy” means any reproduction of an original as defined by this license.

</sentence><sentence>1. </sentence><sentence>OBJECT
</sentence><sentence>The aim of this license is to define the conditions under which one can use this work freely.

</sentence><sentence>2. </sentence><sentence>SCOPE
</sentence><sentence>This work is subject to copyright law. </sentence><sentence>Through this license its author specifies the extent to which you can copy, distribute, and modify it.

</sentence><sentence>2.1 FREEDOM TO COPY (OR TO MAKE REPRODUCTIONS)
</sentence><sentence>You have the right to copy this work for yourself, your friends or any other person, whatever the technique used.

</sentence><sentence>2.2 FREEDOM TO DISTRIBUTE, TO PERFORM IN PUBLIC
</sentence><sentence>You have the right to distribute copies of this work;</sentence><sentence> whether modified or not, whatever the medium and the place, with or without any charge, provided that you:
</sentence><sentence>attach this license without any modification to the copies of this work or indicate precisely where the license can be found,
specify to the recipient the names of the author(s) of the originals, including yours if you have modified the work,
specify to the recipient where to access the originals (either initial or subsequent).
</sentence><sentence>The authors of the originals may, if they wish to, give you the right to distribute the originals under the same conditions as the copies.

</sentence><sentence>2.3 FREEDOM TO MODIFY
</sentence><sentence>You have the right to modify copies of the originals (whether initial or subsequent) provided you comply with the following conditions:</sentence><sentence>
all conditions in article 2.2 above, if you distribute modified copies;</sentence><sentence>
indicate that the work has been modified and, if it is possible, what kind of modifications have been made;</sentence><sentence>
distribute the subsequent work under the same license or any compatible license.</sentence><sentence>
The author(s) of the original work may give you the right to modify it under the same conditions as the copies.

</sentence><sentence>3. </sentence><sentence>RELATED RIGHTS
</sentence><sentence>Activities giving rise to author’s rights and related rights shall not challenge the rights granted by this license.
</sentence><sentence>For example, this is the reason why performances must be subject to the same license or a compatible license.</sentence><sentence> Similarly, integrating the work in a database, a compilation or an anthology shall not prevent anyone from using the work under the same conditions as those defined in this license.

</sentence><sentence>4. </sentence><sentence>INCORPORATION OF THE WORK
</sentence><sentence>Incorporating this work into a larger work that is not subject to the Free Art License shall not challenge the rights granted by this license.
</sentence><sentence>If the work can no longer be accessed apart from the larger work in which it is incorporated, then incorporation shall only be allowed under the condition that the larger work is subject either to the Free Art License or a compatible license.

</sentence><sentence>5. </sentence><sentence>COMPATIBILITY
</sentence><sentence>A license is compatible with the Free Art License provided:</sentence><sentence>
it gives the right to copy, distribute, and modify copies of the work including for commercial purposes and without any other restrictions than those required by the respect of the other compatibility criteria;</sentence><sentence>
it ensures proper attribution of the work to its authors and access to previous versions of the work when possible;</sentence><sentence>it recognizes the Free Art License as compatible (reciprocity);
</sentence><sentence>it requires that changes made to the work be subject to the same license or to a license which also meets these compatibility criteria.

</sentence><sentence>6. </sentence><sentence>YOUR INTELLECTUAL RIGHTS
</sentence><sentence>This license does not aim at denying your author's rights in your contribution or any related right.</sentence><sentence> By choosing to contribute to the development of this common work, you only agree to grant others the same rights with regard to your contribution as those you were granted by this license.</sentence><sentence> Conferring these rights does not mean you have to give up your intellectual rights.

</sentence><sentence>7. </sentence><sentence>YOUR RESPONSIBILITIES
</sentence><sentence>The freedom to use the work as defined by the Free Art License (right to copy, distribute, modify) implies that everyone is responsible for their own actions.

</sentence><sentence>8. </sentence><sentence>DURATION OF THE LICENSE
</sentence><sentence>This license takes effect as of your acceptance of its terms.</sentence><sentence> The act of copying, distributing, or modifying the work constitutes a tacit agreement.</sentence><sentence> This license will remain in effect for as long as the copyright which is attached to the work.</sentence><sentence> If you do not respect the terms of this license, you automatically lose the rights that it confers.
</sentence><sentence>If the legal status or legislation to which you are subject makes it impossible for you to respect the terms of this license, you may not make use of the rights which it confers.

</sentence><sentence>9. </sentence><sentence>VARIOUS VERSIONS OF THE LICENSE
</sentence><sentence>This license may undergo periodic modifications to incorporate improvements by its authors (instigators of the “Copyleft Attitude” movement) by way of new, numbered versions.
</sentence><sentence>You will always have the choice of accepting the terms contained in the version under which the copy of the work was distributed to you, or alternatively, to use the provisions of one of the subsequent versions.

</sentence><sentence>10. </sentence><sentence>SUB-LICENSING</sentence><sentence>
Sub-licenses are not authorized by this license.</sentence><sentence> Any person wishing to make use of the rights that it confers will be directly bound to the authors of the common work.

</sentence><sentence>11. </sentence><sentence>LEGAL FRAMEWORK
</sentence><sentence>This license is written with respect to both French law and the Berne Convention for the Protection of Literary and Artistic Works.

</sentence><sentence>USER GUIDE

</sentence><sentence>- How to use the Free Art License?
</sentence><sentence>To benefit from the Free Art License, you only need to mention the following elements on your work:
</sentence><sentence>[Name of the author, title, date of the work.</sentence><sentence> When applicable, names of authors of the common work and, if possible, where to find the originals].
</sentence><sentence>Copyleft: This is a free work, you can copy, distribute, and modify it under the terms of the Free Art License http://artlibre.org/licence/lal/en/

</sentence><sentence>- Why to use the Free Art License?
</sentence><sentence>1.To give the greatest number of people access to your work.
</sentence><sentence>2.To allow it to be distributed freely.
</sentence><sentence>3.To allow it to evolve by allowing its copy, distribution, and transformation by others.
</sentence><sentence>4.So that you benefit from the resources of a work when it is under the Free Art License:</sentence><sentence> to be able to copy, distribute or transform it freely.
</sentence><sentence>5.But also, because the Free Art License offers a legal framework to disallow any misappropriation.</sentence><sentence> It is forbidden to take hold of your work and bypass the creative process for one's exclusive possession.

</sentence><sentence>- When to use the Free Art License?
</sentence><sentence>Any time you want to benefit and make others benefit from the right to copy, distribute and transform creative works without any exclusive appropriation, you should use the Free Art License.</sentence><sentence> You can for example use it for scientific, artistic or educational projects.

</sentence><sentence>- What kinds of works can be subject to the Free Art License?
</sentence><sentence>The Free Art License can be applied to digital as well as physical works.</sentence><sentence>
You can choose to apply the Free Art License on any text, picture, sound, gesture, or whatever sort of stuff on which you have sufficient author's rights.

</sentence><sentence>- Historical background of this license:
</sentence><sentence>It is the result of observing, using and creating digital technologies, free software, the Internet and art.</sentence><sentence> It arose from the “Copyleft Attitude” meetings which took place in Paris in 2000.</sentence><sentence> For the first time, these meetings brought together members of the Free Software community, artists, and members of the art world.</sentence><sentence> The goal was to adapt the principles of Copyleft and free software to all sorts of creations. </sentence><sentence>http://www.artlibre.org

</sentence><sentence>Copyleft Attitude, 2007.
</sentence><sentence>You can make reproductions and distribute this license verbatim (without any changes). </sentence>
