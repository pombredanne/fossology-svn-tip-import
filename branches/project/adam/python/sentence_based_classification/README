## 
## Copyright (C) 2007 Hewlett-Packard Development Company, L.P.
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## version 2 as published by the Free Software Foundation.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License along
## with this program; if not, write to the Free Software Foundation, Inc.,
## 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
##

The main script for sentence based license classification is f1.py. Its has two functions. First to create a database of labeled sentences. Secondly the script uses the database to label unlabeled license.

Note: The PYTHONPATH environment variable needs to contain the path to utils/parser.py, utils/maxent_utils.py, and sparsevect/python/ to function correctly.

f1.py Usage:
	f1.py [-S sentence_model] [-D database] [-T template_file] [-o output_dir] [-f test_files]
	-S, --sentence_model: path of the sentence model file
	-D, --database: If the -T flag is specified then a database cache file will be saved at the location specified. Otherwise, loads a pre-cached database model.
	-T, --templates: Path to a \\n delineated file with paths to license template files.
	-o, --output: Output directory
	-f, --files: Path to a \\n delineated file with paths to test files
	-d, --debug: Turn debug output on

agent.py is the founation script for a fossology agent. Unlike f1.py the output is provided to stdout in the form of text offsets into files. For example 'GPLv2: [27, 402], [100, 430]'. This means that bytes 27-402 in the target file matched bytes 100-430 in the GPLv2 template file. The other difference is that files are read from the command line. For example '#, /usr/share/doc/python/copyright\n' will test the python copyright file. The '#' is a place holder for the primary key into the fossology database.

agent.py Usage:
	agent.py [-S sentence_model] [-D database] [-T template_file]
	-S, --sentence_model: path of the sentence model file
	-D, --database: If the -T flag is specified then a database cache file will be saved at the location specified. Otherwise, loads a pre-cached database model.
	-T, --templates: Path to a \\n delineated file with paths to license template files.
	-d, --debug: Turn debug output on

A Makefile in the project/adam directory will automatically create the needed model files. Just use the make all rule.

Creating sentence model:
	python utils/sentence_trainner.py --mode train --model_file models.SentenceModel.dat Training/Sentences/*.sent
	

Example: (assume that a sentence model exists at ../models/SentenceModel.dat)
	export PYTHONPATH := ../utils:../sparsevect/python:../sentence_based_classification
	find ../Licenses/ -type f -not \( -regex ".*meta$$" \)  -not \( -wholename "*.svn*" \) > templates.txt

	python f1.py -d -S ../models/SentenceModel.dat -D ../models/Database.dat -T templates.txt
	python f1.py -S ../models/SentenceModel.dat -D ../models/Database.dat -f tests.txt -o output
