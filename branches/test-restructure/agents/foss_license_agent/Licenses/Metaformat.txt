--------------------------------------------------------------
Copyright (C) 2007 Hewlett-Packard Development Company, L.P.
--------------------------------------------------------------

Meta Format

Each license file may be accompanied by a meta file.
   GPLv2
   GPLv2.meta

The ".meta" suffix denotes a meta file.
Each meta file contains "field: value" pairs.
The delimiation is a ": " (colon followed by optional whitespace).
The value ends with a newline.
White space at the end of the line is removed.

Any line beginning with a "#" is a comment.

ALL fields are optional.
The following fields are defined:
	URL: Defines a URL to the license source.
	Source: Text string describing where it came from.
	  This could be a package name or something else.
	Date: Date associated with the license.
	  If the actual date for the license is known, then use it.
	  It should be no later than the date the license was added to
	  the Raw directory.
	  The format should either be: YYYY-MM-DD or "YYYY-MM-DD hh:mm:ss".
	  (Any SQL date format should work.)
	Alias: The RAW directory should have a unique name.
	  These are aliases for the unique name.
	  Unlike the unique name, the alias does not need to be unique.
	  E.g. for GPLv2:
	    Alias: GNU Public License version 2
	    Alias: GNU Public License
	    Alias: GPL
	  Every GPL version and reference will likely have "Alias: GPL".

Technical details:
  - "Date:" gets put in agent_lic_raw.lic_date
  - "URL:" gets put in agent_lic_raw.lic_url
  - "Source:" gets put in agent_lic_raw.lic_source
  - "Alias:" gets put in ???  (There is a many-to-one relationship.)

"Alias:" may have more than one meta entries.
"Date:", "URL:", and "Source:" may only have one each.  If there are multiple
entries, then the last one wins.  (DB value will be updated multiple times,
updating each.)

If a license file has multiple sections, then the meta file will be
re-processed once per section.  (No caching.)

======================================================================
Common locations for lots of licenses:
http://fedoraproject.org/wiki/Licensing
http://opensource.org/licenses/
